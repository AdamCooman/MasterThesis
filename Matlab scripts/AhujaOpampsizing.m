%%ontwerp opamp thesis


%% Process parameters

n=1.3;
KPMOS=93.9;
KNMOS=258;

%% Specs

GBW=100e6;
Cl=10e-12;

%% Design

a=3;
b=3;
g=5;

%% input transistor Min

Min.gm=Cl*2*pi*GBW/a;
Min.Vov=0.12;
Min.gmoverId=2/Min.Vov;
Min.Id=Min.gm/Min.gmoverId;
Min.WoL=2*1e6*n*Min.Id/KPMOS/Min.Vov/Min.Vov;

%% differential tail transistor Mtop

Mtop.Id=2*Min.Id;
Mtop.Vov=0.2;
Mtop.WoL=2*1e6*n*Mtop.Id/KPMOS/Mtop.Vov/Mtop.Vov;

%% output transistor Mout

Mout.gm=2*pi*g*GBW*Cl*(1+1/a)/b;
Mout.Vov=0.2;
Mout.gmoverId=2/Mout.Vov;
Mout.Id=Mout.gm/Mout.gmoverId;
Mout.WoL=2*1e6*n*Mout.Id/KNMOS/Mout.Vov/Mout.Vov;

%% output load transistor Mload

Mload.Id=Mout.Id;
Mload.Vov=0.2;
Mload.WoL=2*1e6*n*Mload.Id/KPMOS/Mload.Vov/Mload.Vov;

%% mirror transistor and common mode feedback transistor Mmirr

Mmirr.gm=Min.gm;
Mmirr.Vov=0.2;
Mmirr.gmoverId=2/Mmirr.Vov;
Mmirr.Id=Mmirr.gm/Mmirr.gmoverId;
Mmirr.WoL=2*1e6*n*Mmirr.Id/KPMOS/Mmirr.Vov/Mmirr.Vov;

%% Current buffer cascode Mcasctop

Mcasctop.Id=Mmirr.Id;
Mcasctop.Vov=0.15;
Mcasctop.WoL=2*1e6*n*Mcasctop.Id/KPMOS/Mcasctop.Vov/Mcasctop.Vov;

%% Current buffer cascode Mcasc

Mcasc.Id=Mmirr.Id;
Mcasc.Vov=0.15;
Mcasc.WoL=2*1e6*n*Mcasc.Id/KNMOS/Mcasc.Vov/Mcasc.Vov;

%% Bottom transistor Mbottom

Mbottom.Id=Min.Id+Mmirr.Id;
Mbottom.Vov=0.2;
Mbottom.WoL=2*1e6*n*Mbottom.Id/KNMOS/Mbottom.Vov/Mbottom.Vov;

%% Common-mode specs

Clcm=1e-12;
d=5;

%% Common-mode input transistor Mcmin

Mcmin.gm=d*GBW*Clcm*2*pi;
Mcmin.Vov=Mmirr.Vov;
Mcmin.gmoverId=2/Mcmin.Vov;
Mcmin.Id=Mcmin.gm/Mcmin.gmoverId;
Mcmin.WoL=2*1e6*n*Mcmin.Id/KNMOS/Mcmin.Vov/Mcmin.Vov;

%% Common-mode load transistor Mcmload

Mcmload.gm=Mcmin.gm;
Mcmload.Vov=Mmirr.Vov;
Mcmload.Id=Mcmin.Id;
Mcmload.WoL=2*1e6*n*Mcmload.Id/KPMOS/Mcmload.Vov/Mcmload.Vov;

%% Common mode bottom transistor Mcmbot

Mcmbot.Id=Mcmin.Id*2;
Mcmbot.Vov=Mbottom.Vov;
Mcmbot.WoL=2*1e6*n*Mcmbot.Id/KNMOS/Mcmbot.Vov/Mcmbot.Vov;

%% Linear region NMOS transistor for bias Mcasclin
Margin=0.05;
Mcasclin.WoL=1e6*Mmirr.Id*n/KNMOS/(0.5*Mbottom.Vov+0.5*Margin+Mcasc.Vov)/(Mbottom.Vov+Margin);

%% Linear region PMOS transistor for bias
Mcasctoplin.WoL=1e6*Mmirr.Id*n/KPMOS/(0.5*Mmirr.Vov+0.5*Margin+Mcasctop.Vov)/(Mmirr.Vov+Margin);

