%% step response in function of gamma
clear all
close all

global makepdfs
makepdfs=true;

GBW=100e6; %Hz
DC=60; %dB
freqas=logspace(1,10,1000);
time=linspace(0,1e-6,1000);

gval=[1.5 2 2.5 3 4 6];
g={};

for ii=1:length(gval)

p1=GBW/10^(DC/20);
p2=gval(ii)*GBW;
den=conv([1 p1],[1 p2]);
num=10^(DC/20)*den(end);
g{ii}.opamp.tf=tf(num,den);
[g{ii}.opamp.mag,g{ii}.opamp.phase]=bode(g{ii}.opamp.tf,freqas);
g{ii}.buffer.tf=g{ii}.opamp.tf/(1+g{ii}.opamp.tf);
[g{ii}.buffer.mag,g{ii}.buffer.phase]=bode(g{ii}.buffer.tf,freqas);
[g{ii}.buffer.step,t] = step(g{ii}.buffer.tf,time);
end

figure

semilogx(freqas,db(squeeze(g{1}.opamp.mag)))
hold on
for ii=2:length(gval)
    if gval(ii)==3
        semilogx(freqas,db(squeeze(g{ii}.opamp.mag)),'r','linewidth',2) 
    elseif gval(ii)==4
        semilogx(freqas,db(squeeze(g{ii}.opamp.mag)),'m','linewidth',2)
    elseif gval(ii)==2
        semilogx(freqas,db(squeeze(g{ii}.opamp.mag)),'g','linewidth',2)
    else
        semilogx(freqas,db(squeeze(g{ii}.opamp.mag)))
    end
end
grid on


figure('name','buffer frequency response') 
semilogx(freqas,db(squeeze(g{1}.buffer.mag)))
hold on
for ii=2:length(gval)
    if gval(ii)==3
        semilogx(freqas,db(squeeze(g{ii}.buffer.mag)),'r','linewidth',2) 
    elseif gval(ii)==4
        semilogx(freqas,db(squeeze(g{ii}.buffer.mag)),'m','linewidth',2)
    elseif gval(ii)==2
        semilogx(freqas,db(squeeze(g{ii}.buffer.mag)),'g','linewidth',2)
    else
        semilogx(freqas,db(squeeze(g{ii}.buffer.mag)))
    end
end
grid on
ylim([-4 0.5])
xlim([5e6 1.5e8])
xlabel('frequency [Hz]')
ylabel('[dB]')
annotation('textarrow',[0.397301349325337 0.67616191904048],...
    [0.770897832817337 0.808049535603715],'TextEdgeColor','none',...
    'String','\gamma = 2');
annotation('textarrow',[0.506746626686657 0.671664167916042],...
    [0.691950464396285 0.747678018575851],'TextEdgeColor','none',...
    'String','\gamma = 3');
annotation('textarrow',[0.590704647676162 0.706146926536732],...
    [0.606811145510836 0.665634674922601],'TextEdgeColor','none',...
    'String','\gamma = 4');

% makepdf('freq response')


figure('name','step response')
plot(time,squeeze(g{1}.buffer.step))
hold on
for ii=2:length(gval)
    if gval(ii)==3
        plot(time,squeeze(g{ii}.buffer.step),'r','linewidth',2) 
    elseif gval(ii)==4
        plot(time,squeeze(g{ii}.buffer.step),'m','linewidth',2)
    elseif gval(ii)==2
        plot(time,squeeze(g{ii}.buffer.step),'g','linewidth',2)
    else
        plot(time,squeeze(g{ii}.buffer.step))
    end
end
ylim([0.88 1.09])
xlim([1.6e-8 10e-8])
xlabel('time [s]')
ylabel('output voltage [V]')

x = [0.469642857142857 0.3];
y = [0.4 0.53];
annotation('textarrow',x,y,'String','\gamma = 4','FontSize',10)

x=[0.469642857142857 0.291071428571429];
y=[0.688095238095238 0.597619047619048];
annotation('textarrow',x,y,'String','\gamma = 3','FontSize',10)
           
x=[0.467857142857143 0.289285714285714];
y=[0.845238095238095 0.746666666666668];
annotation('textarrow',x,y,'String','\gamma = 2','FontSize',10)
                  
grid on
% makepdf('step response')

    