\documentclass[compress]{beamer}

\usetheme[]{Luebeck}
%\usetheme[]{Madrid}
%\usetheme[]{AnnArbor}

\useoutertheme{split}
\usecolortheme{vub}
\usefonttheme[onlymath]{serif}
\usepackage{marvosym}
\usepackage{amssymb}
\usepackage{pifont}
\usepackage{listings}

\setbeamertemplate{navigation symbols}{}

\input{settings.inc.tex}

\logo{\includegraphics[width=3cm]{VUB_schild}}

\title[Master's thesis \hspace{14em} \textbf{\insertpagenumber} / \insertdocumentendpage]
                      {Design and Analysis of an On-Chip\\
                       Programmable Op-Amp based Filter}
\author{Adam Cooman}
\institute{Vrije Universiteit Brussel}
\date{June 27, 2012}
%\pgfdeclareimage[height=1.2cm]{mylogo}{VUB_schild}

\begin{document}

\begin{frame}
\includegraphics[height=1cm]{VUB_logo}
\titlepage
%\begin{center}
  \begin{tabular}{ll}
  \textbf{Promotor} & Prof. Dr. ir. Gerd Vandersteen\\
  \end{tabular}
%\end{center}
%\vspace{-5cm}\hspace{7cm}\includegraphics[width=3.5cm]{VUB_schild}
\end{frame}
\logo{}

%\frame{\tableofcontents}

\section{Introduction}
\begin{frame}{Introduction}
\vspace{0cm}
  \only<1>{\framesubtitle{Linear system}}
  \includegraphics<1>[width=\columnwidth]{sys-000-lin.pdf}
  \note<1>{Bij het ontwerpen van schakelingen (of andere systemen), werkt men
           meestal eerst met een lineair model van het systeem. Dit is een eenvoudige
           voorstelling, die vaak goed werk. Als we bv. de respons van een lineair
           systeem op een sinus (hier in frequentiedomein en tijdsdomein weergegeven),
           bekijken, merken we dat dit weer een sinus is met dezelfde frequentie
           -- maar mogelijk een verschillende amplitude en fase. We weten echter
           dat echte lineaire systemen niet bestaan, designers van schakelingen
           zullen dus ook met completere modellen hun systeem trachten te verifiëren.
           Systeem lineair proberen houden.}
  
  \only<2>{\framesubtitle{(Strongly) Nonlinear system}\transdissolve}
  \includegraphics<2>[width=\columnwidth]{sys-001-snlin.pdf}
  \note<2>{Uit simulaties/metingen blijkt: geen perfecte sinus aan de uitgang.
           In veel gevallen: componenten op $n\cdot f_1$. Dit vb. is het gevolg
           van een sterke niet-lineariteit, gaan we hier niet verder behandelen.}

  \only<3>{\framesubtitle{(Weakly) Nonlinear system}\transdissolve}
  \includegraphics<3>[width=\columnwidth]{sys-002-wnlin.pdf}
  \note<3>{Subtieler geval waar de niet-lineariteiten zwak zijn. Definitie WNLin: zie verder.
           Te zien aan de stompe pieken van de uitgang in dit geval.}
\end{frame}

\begin{frame}{Introduction}{Typical measures of nonlinearity}
\begin{columns}
 \begin{column}{0.5\textwidth}
   \begin{itemize}
  \item[\itm] Harmonic Distortion
  \[
    \HD_n = \frac{\abs{Y\left(n\cdot f_1\right)}}{\abs{Y(f_1)}}
  \]
  \item[\itm] Total Harmonic Distortion
  \[
    \THD = \sqrt{\sum_{n=2}^{\infty} \HD_n^2}
  \]

  \item[\itm] Intercept Points


 \end{itemize}
 \end{column}
 \begin{column}{0.5\textwidth}
 \includegraphics[width=\columnwidth]{systems-FD.pdf}
 
 \end{column}
 

\end{columns}
\begin{itemize}
\item[\con] {\color{TangoScarletRed3} Only a single numerical value}
\begin{itemize}
\item How to improve nonlinear behavior?
\end{itemize}
\end{itemize}




\end{frame}


\begin{frame}{Introduction}{Goal of the simulation method}
  \begin{itemize}
    \item Pin-point dominant nonlinear contributions in circuit
    \includegraphics[width=\columnwidth]{opamp-000.pdf}
  \end{itemize}
  \note{
        \begin{itemize}
         \item In de praktijk is een systeem of schakeling vaak redelijk ingewikkeld
         \item Bepalen: waar komen de grootste niet-lineariteiten vandaan?
        \end{itemize}
       }
\end{frame}


\section{Theory}
  \subsection{Volterra Theory}
  \begin{frame}{Use of Volterra theory for weakly nonlinear systems}
  For nonlinear dynamic systems: Volterra series
    \[
     y(t) = \sum_{n=1}^{\infty} \frac{1}{n!}
     \intdotsRR h_n(\tau_1,\ldots,\tau_n)
     \prod_{j=1}^{n} u(t-\tau_j) \dd{\tau_1} \cdots \dd{\tau_n}
    \]
    \begin{itemize}
      \item Consider only $n \leq 3$ $\Rightarrow$ weakly nonlinear
      \vspace{1em}
      \begin{columns}[t]
      \begin{column}{0.5\textwidth}
      Convolution ($n=1 \Rightarrow$ Linear)
      \[
        y(t) = \intRR h(\tau) u(t-\tau) \dd{\tau}
        \]
        \end{column}
        \begin{column}{0.5\textwidth}
        Taylor series ($h_n$ static)
        \[
          y(u) = \sum_{n=1}^{\infty} \frac{1}{n!} \deriv[n]{y}{u} u^n
          \]
          \end{column}
          \end{columns}
          \item Volterra kernels:
          $h_n(\tau_1, \tau_2, \ldots, \tau_n)
          \; \stackrel{\mathcal{F}}{\longleftrightarrow} \;
          H_n(f_1, f_2, \ldots, f_n)$
    \end{itemize}
  \end{frame}

  \begin{frame}{Simulation strategy}{Determining response order by order}
  \begin{columns}
   \begin{column}{0.45\textwidth}
   \textbf{First order (linear)}
   \begin{itemize}
    \item $v_{in}$ $\to$ $v_{out}$, $v_{c}$
    \item No nonlinear sources
   \end{itemize}
   
   \vspace{2em}
   
   \textbf{Higher orders (nonlinear)}
   \begin{itemize}
   \item $n^{\text{th}}$ order depends on orders $1,\ldots,n-1$
   \item $I_{n{K_m}}$ $\to$ $v_{out}$, $v_{c}$
   \item Input: linear $\Rightarrow$ removed
   \end{itemize}
    
   \end{column}
   \begin{column}{0.6\textwidth}
   \includegraphics[width=\columnwidth]{strat-lin.pdf}
   
   \vspace{3em}
   
   \includegraphics[width=\columnwidth]{strat-nlin.pdf}
   \end{column}
  \end{columns}
\end{frame}
  

\subsection{Algorithm}
\begin{frame}{Algorithm}
\begin{itemize}
\item[] \texttt{for} order $n = \left\{ 2,3\right\}$
  \begin{itemize}
    \item[] \texttt{foreach} nonlinearity $m$ of order at most $n$
      \begin{itemize}
       \item[] calculate nonlinear current source (controlled by $c$):
         \[
           {\color{TangoOrange2} I_{nK_m}} =
           \function\!\left({\color{TangoChameleon3} H_{1(c)}}, \ldots, H_{n-1(c)} \right)
           \cdot
           {\color{TangoScarletRed2} K_m}
         \]
      \end{itemize}
      \item[] \texttt{for} node $x = \left\{\text{output nodes}, \text{controlling nodes}\right\}$
      \begin{itemize}
       \item[] calculate Volterra kernel of order $n$ at node $x$:
         \[
           H_{n(x)} = \sum_{\substack{\text{All nonlinear sources}\\{\text{$m$ of order $n$}}}} \!\!\!\!
           \overbrace{
             {\color{TangoOrange2} I_{nK_m}} \cdot
             {\color{TangoSkyBlue2}\mathrm{tf}_{I_{nK_m} \to x}}
             }^{\text{single contribution}}
         \]
      \end{itemize}
  \end{itemize}
  \item[] Calculate voltage at node $x$ at frequency $k_1f_1 + k_2f_2 + \ldots$
    \[
      V_{x[k_1,k_2,\ldots]} =
        \function\!\left(v_{in},{\color{TangoChameleon3}H_{1(x)}}, H_{2(x)}, H_{3(x)}\right)
    \]
\end{itemize}

\end{frame}


  \subsection{MOSFETs}
  \begin{frame}{MOSFETs}
    \begin{columns}
      \begin{column}{0.4\textwidth}
      \includegraphics<1>[width=1.1\columnwidth]{mosterminals.pdf}
        \begin{align*}
        i_{DS}(t) & = \overbrace{I_{DS}}^{DC} + i_{ds}(t)\\
        i_{DS}\phantom{(t)} &= \function \left(v_{GS},v_{DS},v_{SB}\right)
        \end{align*}
        $f$: nonlinear function

      \end{column}
      \begin{column}{0.6\textwidth}
        Taylor of $i_{DS}$ around $(V_{GS},V_{DS},V_{SB})$:
        \begin{align*}
        i_{ds} &=  g_m v_{gs}    + g_0 v_{ds}    - g_{mb} v_{sb} +
        {\color{TangoScarletRed2}K_{2 g_m}} v_{gs}^2\\
     &+ {\color{TangoScarletRed2}K_{2 g_0}} v_{ds}^2
      - {\color{TangoScarletRed2}K_{2 g_{mb}}} v_{sb}^2
      + {\color{TangoScarletRed2}K_{2 g_m g_0}} v_{gs}v_{ds}\\
     &+ {\color{TangoScarletRed2}K_{2 g_m g_{mb}}} v_{gs}v_{sb}
      + {\color{TangoScarletRed2}K_{2 g_0 g_{mb}}} v_{sb}v_{ds} \\
     &+ {\color{TangoScarletRed2}K_{3 g_m}} v_{gs}^3
      + {\color{TangoScarletRed2}K_{3 g_0}} v_{ds}^3
      - {\color{TangoScarletRed2}K_{3 g_{mb}}} v_{sb}^3  \\
     &+ {\color{TangoScarletRed2}K_{3 g_m g_m g_0}} v_{gs}^2v_{ds}
      + {\color{TangoScarletRed2}K_{3 g_m g_m g_{mb}}} v_{gs}^2v_{sb}\\
     &+ {\color{TangoScarletRed2}K_{3 g_m g_{mb} g_{mb}}} v_{sb}^2v_{ds}
      + {\color{TangoScarletRed2}K_{3 g_m g_0 g_0}} v_{gs}v_{ds}^2 \\
     &+ {\color{TangoScarletRed2}K_{3 g_m g_{mb} g_{mb}}} v_{gs}v_{sb}^2
      + {\color{TangoScarletRed2}K_{3 g_0 g_0 g_{mb}}} v_{sb}v_{ds}^2\\
     &+ {\color{TangoScarletRed2}K_{3 g_m g_0 g_{mb}}} v_{gs} v_{ds} v_{sb} + \ldots\\
    \end{align*}
      $6+10$ contributions per transistor
      \end{column}
    \end{columns}
  \end{frame}

\section{Implementation}
\subsection{Overview}
\begin{frame}{Implementation}{Overview}
  \includegraphics[width=\columnwidth]{./schema.pdf}
\end{frame}

\subsection{Parser}
\begin{frame}[fragile]{Implementation}{Spectre (SCS) Netlist Parser}
\begin{itemize}
\item Input for analysis: circuit (and excitation)
\item Netlist file (text) $\leftrightarrow$ circuit description (tree of objects)
\item Implementation: \antlr\ \& Java
\end{itemize}
 \begin{columns}
  \begin{column}{0.75\textwidth}

   \lstinputlisting[style=scs]{example.scs}
  \end{column}

  \begin{column}{0.25\textwidth}
     \begin{figure}\centering
     \includegraphics[width=\columnwidth]{./example.pdf}
     \end{figure}     
  \end{column}
\end{columns}
\end{frame}

\subsection{Simulations}
\begin{frame}{Simulations}
  \only<1>{\framesubtitle{Circuit under test: fully differential Miller-opamp}}
  \includegraphics<1>[width=1.1\columnwidth]{opamp-000.pdf}
  
  \only<2>{\subsection{Linear}\framesubtitle{Apply input source (+ biasing)}\transdissolve}
  \includegraphics<2>[width=1.1\columnwidth]{opamp-001.pdf}

  \only<3>{\framesubtitle{Obtain linear Volterra kernel $H_1(s)$ (FRF) to all nodes}\transdissolve}
  \includegraphics<3>[width=1.1\columnwidth]{opamp-002.pdf}

  \only<4>{\framesubtitle{Remove input source (leave biasing intact)}\transdissolve}
  \includegraphics<4>[width=1.1\columnwidth]{opamp-003.pdf}

  \only<5>{\subsection{Nonlinear}\framesubtitle{Apply only a single nonlinear current source}\transdissolve}
  \includegraphics<5>[width=1.1\columnwidth]{opamp-004.pdf}

  \only<6>{\framesubtitle{Obtain linear FRF from the current source to all node voltages}\transdissolve}
  \includegraphics<6>[width=1.1\columnwidth]{opamp-005.pdf}

  \only<7>{\framesubtitle{Repeat for every nonlinearity}\transdissolve}
  \includegraphics<7>[width=1.1\columnwidth]{opamp-006.pdf}
  
\end{frame}

\subsection{Results}
\begin{frame}{Results}
   Results for $V_{out[3,0]}$ (third harmonic) of opamp
   \begin{itemize}
    \item Runtime\footnote{Core i7 860 ($2.8 - 3.4 \mathrm{GHz}$), $8\mathrm{GB}$ RAM, Ubuntu 10.10 (64-bit)}: $7 \text{s}$
    \item Number of contributions: $44${\color{TangoAluminium4}/$141$/$240=15\cdot16$}
    \item Main contributions:\vspace{1ex}
      
      \begin{tabular}{|rl|r|r|r|r|} \hline
      \textbf{Transistor}  &\textbf{Mechanism} &\textbf{Contribution}  &\textbf{Cumulative}\\ \hline
      Mn3b    &$K_{3{g_m g_0 g_0}}$   &$29.4 \%$  &$29.4 \%$\\ \hline
      Mn3a    &$K_{3{g_m g_0 g_0}}$   &$29.4 \%$  &$58.8 \%$\\ \hline
      Mn3b    &$K_{3{g_m g_m g_0}}$   &$13.2 \%$  &$72.0 \%$\\ \hline
      Mn3a    &$K_{3{g_m g_m g_0}}$   &$13.2 \%$  &$85.3 \%$\\ \hline
      Mn3b    &$K_{3{g_m}}$           &$-13.1 \%$ &$72.2 \%$\\ \hline
      Mn3a    &$K_{3{g_m}}$           &$-13.1 \%$ &$59.1 \%$\\ \hline
      Mn2b    &$K_{3{g_0}}$           &$9.6 \%$   &$68.7 \%$\\ \hline
      \end{tabular}
   \end{itemize}
\end{frame}

\begin{frame}{Results}
  \includegraphics[width=1\columnwidth]{opamp.pdf}
\end{frame}

\subsection{Validation}
\begin{frame}{Validation}
  \begin{itemize}
   \item[\pro] Simple circuits
   \begin{itemize}
     \item Common source
     \item Differential pair
   \end{itemize}
   \item[\pro] More complex circuit
     \begin{itemize}
      \item Opamp (see before)
     \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Validation}{Common source amplifier}
  \begin{itemize}
    \item[\pro] Compared ($V_{out[2,0]}, V_{out[3,0]}$) to Volterra results in literature
    \begin{columns}
    \begin{column}{0.25\textwidth}
      \includegraphics<1>[height=10em]{commonsource.pdf}
    \end{column}
    \begin{column}{0.75\textwidth}
    \begin{itemize}
      \item[\pro] Relative error $\leq 10^{-4}$
    \end{itemize}
    \begin{tabular}{|l|r|r|} \hline
    \textbf{Mechanism} &\textbf{Automated}       & \textbf{Literature}\\ \hline
    $K_{2{g_m}}$       & $-447.31\nVolt$         & $-447.31\nVolt$
    \\ \hline
    $K_{2{g_m g_0}}$   & $176.66\nVolt$          & $176.67\nVolt$\\ \hline
    $K_{2{g_0}}$       & $48.43\nVolt$          & $48.44\nVolt$\\ \hline
    \textbf{Total $V_{out[2,0]}$}     & $\mathbf{-222.23\nVolt}$& $\mathbf{-222.20\nVolt}$\\ \hline \hline
    
    \textbf{Total $V_{out[3,0]}$} & $\mathbf{77.21\pVolt}$ & $\mathbf{77.21\pVolt}$\\ \hline
    \end{tabular}
    \vspace{1ex}
    \end{column}
  \end{columns}
    \item[\pro] Compared with Harmonic Balance
      \begin{itemize}
      \item[\con] Difference of $2\dB$ to $6\dB$
      \item[\wrn] Cause: limited accuracy of $K$-coefficients due to interpolation
     \end{itemize}      
  \end{itemize}
\end{frame}

\begin{frame}{Validation}{Differential pair}
\begin{columns}
  \begin{column}{0.4\textwidth}
    \includegraphics<1>[height=15em]{diffpair.pdf}
  \end{column}
  \begin{column}{0.7\textwidth}
    \begin{itemize}
     \item Balanced operation
     \begin{itemize}
      \item[\itm] Circuit is symmetric
      \item[\pro] Even harmonics should cancel
      \item[\wrn] Numerical precision
     \end{itemize}
     \item Mismatched: $1\%$ mismatch on resistance
     \begin{itemize}
      \item[\itm] $R_1 = 0.99 R \qquad R_2 = 1.01 R$
      \item[\pro] Small even harmonics
     \end{itemize}

    \end{itemize}

  \end{column}
\end{columns}
\end{frame}



\begin{frame}{Validation}{Differential pair}
\vspace{1ex}
  \begin{itemize}
  \item Contributions to $V_{out[2,0]}$:
   \begin{tabular}{|rl|r|r|} \hline
   \textbf{Transistor}& \textbf{Mechanism}&\textbf{Matched} & \textbf{Mismatch}\\ \hline
   Mp1b    &$K_{2{g_m}}$           &{\color{TangoChameleon3}$ 353.43\uVolt$}   & $ 352.07\uVolt$\\ \hline
   Mp1a    &$K_{2{g_m}}$           &{\color{TangoScarletRed3}$-353.43\uVolt$}   & $-354.78\uVolt$\\ \hline
   Mp1b    &$K_{2{g_m g_0}}$       &{\color{TangoChameleon3}$-79.64\uVolt$}    & $ -78.57\uVolt$\\ \hline
   Mp1a    &$K_{2{g_m g_0}}$       &{\color{TangoScarletRed3}$ 79.64\uVolt$}    & $  80.71\uVolt$\\ \hline
   Mp1b    &$K_{2{g_0}}$           &{\color{TangoChameleon3}$-6.04\uVolt$}    & $  -5.92\uVolt$\\ \hline
   Mp1a    &$K_{2{g_0}}$           &{\color{TangoScarletRed3}$ 6.04\uVolt$}     & $   6.16\uVolt$\\ \hline
   Mp1b    &$K_{2{g_m g_{mb}}}$    &$0.01\mathrm{aV}$ & $ -78.65\nVolt$\\ \hline
   Mp1a    &$K_{2{g_m g_{mb}}}$    &$0.01\mathrm{aV}$ & $ -78.31\nVolt$\\ \hline
   \textbf{Total}  &   &\textbf{$\mathbf{0.31\mathrm{aV}}$} & \textbf{$\mathbf{-471.37\nVolt}$}\\ \hline
   \end{tabular}\vspace{1ex}
   \item[\pro] Matching contributions can be removed automatically
   \item[\wrn] Manual interpretation still needed
  \end{itemize}

\end{frame}



\section{Overview}
\begin{frame}{Overview}{What to remember?}
  \begin{itemize}
  \item[\pro] Automated Volterra-based analysis
  \item[\pro] Pin-point the dominant contributions
  \item[\pro] SCS netlist parser \& generator
  \item[\pro] Object-Oriented (Java \& \matlab)
  \end{itemize}
\end{frame}

\end{document}
